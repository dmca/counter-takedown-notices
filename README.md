# Counter Takedown Notices

This repository stores all DMCA takedown counter notices by request. Please refer to our [DMCA Counter Notice Guide](https://www.codelinaro.org/policies/guide-to-submitting-a-dmca-counter-notice.html) for further information.

~ CodeLinaro Support
